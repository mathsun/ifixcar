﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace IFixCar_.Models
{
    public class ServiceCategory
    {

        public int id { get; set; }
        [Display(Name = "Service Name")]
        public string ServiceName { get; set; }
        public string SvImg { get; set; }
    }
}
