﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IFixCar_.Models
{
    public class Users : IdentityUser
    {
        public Users() : base() { }
        public string Fullname { set; get; }
        public DateTime Birthday { set; get; }
        public string Gender { set; get; }

    }
}
