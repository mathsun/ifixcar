﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using IFixCar_.Views.Admin;
using Microsoft.AspNetCore.Authorization;

namespace IFixCar_.Controllers
{
    public class AdminController : Controller
    {
       
        [Authorize(Roles = "Admin")]
        public IActionResult Dashboard()
        {
            return View();
        }
    }
}