﻿using IFixCar_.Data;
using IFixCar_.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IFixCar_.Seeders
{
    public static class DataSeeder
    {
        public static async Task Initialize(ApplicationDbContext context, RoleManager<Roles> RoleManager, UserManager<Users> userManager)
        {
            context.Database.EnsureCreated();

            String admin1 = "";

            string role1 = "Admin";
            string role2 = "Manager";
            string role3 = "Customer";
            string password = "Abc@123";


            // Roles Seeder
            if ( await RoleManager.FindByNameAsync(role1) == null)
            {
                await RoleManager.CreateAsync(new Roles(role1,role1));
            }
            if( await RoleManager.FindByNameAsync(role2) == null)
            {
                await RoleManager.CreateAsync(new Roles(role1,role2));
                
            }
            if (await RoleManager.FindByNameAsync(role3) == null)
            {
                await RoleManager.CreateAsync(new Roles(role1, role3));
            }
            // Admin Account Seeder
            if (await userManager.FindByEmailAsync("admin@admin.com") == null)
            {
                var user = new Users { UserName = "admin@admin.com", Email = "admin@admin.com", Birthday = DateTime.UtcNow.Date, Fullname = "TheLegend", Gender = "Shemale" };
                var result = await userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user,password);
                    await userManager.AddToRoleAsync(user, role1);
                }
                admin1 = user.Id;
            }
            // Manager Account Seeder
            if (await userManager.FindByEmailAsync("manager@manager.com") == null)
            {
                var user = new Users { UserName = "manager@manager.com", Email = "manager@manager.com", Birthday = DateTime.UtcNow.Date, Fullname = "Flawless", Gender = "Alicoper" };
                var result = await userManager.CreateAsync(user, password);
                if (result.Succeeded)
                {
                    await userManager.AddPasswordAsync(user, password);
                    await userManager.AddToRoleAsync(user, role2);
                }
            }
        }    
    }
}
