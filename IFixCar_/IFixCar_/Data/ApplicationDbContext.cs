﻿using System;
using System.Collections.Generic;
using System.Text;
using IFixCar_.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace IFixCar_.Data
{
    public class ApplicationDbContext : IdentityDbContext<Users,Roles, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<IFixCar_.Models.ServiceCategory> ServiceCategory { get; set; }
    }
}
