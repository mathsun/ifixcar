﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IFixCar_.Data.Migrations
{
    public partial class Newclumnforcategorytb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte[]>(
                name: "SvImg",
                table: "ServiceCategory",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SvImg",
                table: "ServiceCategory");
        }
    }
}
