﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IFixCar_.Data.Migrations
{
    public partial class c : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SvImg",
                table: "ServiceCategory",
                nullable: true,
                oldClrType: typeof(byte[]),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "SvImg",
                table: "ServiceCategory",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
