﻿Create table Product (
RegisteredServiceId int identity primary key,
ServiceName varchar(50),
ServiceDescription varchar(999),
ServicePrice float,
IsAvailable bit null,
CreatedDate datetime null,
ModifiedDate datetime null,
foreign key (RegisteredServiceId) references ServiceCategory(id)
)